---
v: 3

#title: Using the Static Context Header Compression (SCHC) with Proxies for the Constrained Application Protocol (CoAP)
title: Clarifications and Updates on using Static Context Header Compression (SCHC) for the Constrained Application Protocol (CoAP)
#abbrev: SCHC with CoAP Proxies
abbrev: Updates on using SCHC for CoAP
docname: draft-tiloca-lpwan-8824-update-latest

# stand_alone: true

ipr: trust200902
area: Internet
wg: LPWAN Working Group
kw: Internet-Draft
cat: std
submissiontype: IETF
updates: 8824

coding: utf-8
pi:    # can use array (if all yes) or hash here

  toc: yes
  sortrefs:   # defaults to yes
  symrefs: yes

author:
      -
        ins: M. Tiloca
        name: Marco Tiloca
        org: RISE AB
        street: Isafjordsgatan 22
        city: Kista
        code: SE-16440
        country: Sweden
        email: marco.tiloca@ri.se
      -
        ins: L. Toutain
        name: Laurent Toutain
        org: IMT Atlantique
        street: CS 17607, 2 rue de la Chataigneraie
        city: Cesson-Sevigne Cedex
        code: 35576
        country: France
        email: Laurent.Toutain@imt-atlantique.fr
      -
        ins: I. Martinez
        name: Ivan Martinez
        org: IMT Atlantique
        street: CS 17607, 2 rue de la Chataigneraie
        city: Cesson-Sevigne Cedex
        code: 35576
        country: France
        email: ivanmarinomartinez@gmail.com

normative:
  RFC2119:
  RFC7252:
  RFC7959:
  RFC8174:
  RFC8724:
  RFC8613:
  RFC8768:
  RFC8824:
  RFC9175:
  RFC9177:
  I-D.ietf-core-oscore-edhoc:
  I-D.ietf-core-oscore-groupcomm:
  I-D.ietf-core-oscore-key-update:

informative:
  I-D.ietf-core-groupcomm-bis:
  I-D.ietf-lake-edhoc:

--- abstract

This document clarifies, updates and extends the method specified in RFC 8824 for compressing Constrained Application Protocol (CoAP) headers using the Static Context Header Compression and fragmentation (SCHC) framework. In particular, it considers recently defined CoAP options and specifies how CoAP headers are compressed in the presence of intermediaries. Therefore, this document updates RFC 8824.

--- middle

# Introduction # {#intro}

The Constrained Application Protocol (CoAP) {{RFC7252}} is a web-transfer protocol intended for applications based on the REST (Representational State Transfer) paradigm, and designed to be affordable also for resource-constrained devices.

In order to enable the use of CoAP in LPWANs (Low-Power Wide-Area Networks) as well as to improve performance, {{RFC8824}} defines how to use the Static Context Header Compression and fragmentation (SCHC) framework {{RFC8724}} for compressing CoAP headers.

This document clarifies, updates and extends the SCHC compression of CoAP headers defined in {{RFC8824}} at the application level, by: providing specific clarifications; updating specific details of the compression processing, based on recent developments related to the security protocol OSCORE {{RFC8613}} for end-to-end protection of CoAP messages; and extending the compression processing to take into account additional CoAP options and the presence of CoAP proxies.

In particular, this document updates {{RFC8824}} as follows.

* It clarifies the SCHC compression for the CoAP options Size1, Size2, Proxy-URI and Proxy-Scheme (see {{coap-options-updated-set}}).

* It defines the SCHC compression for the CoAP option Hop-Limit (see {{coap-options-hop-limit}}).

* It defines the SCHC compression for the recently defined CoAP options Echo (see {{coap-options-echo}}), Request-Tag (see {{coap-options-request-tag}}), EDHOC (see {{coap-options-edhoc}}), as well as Q-Block1 and Q-Block2 (see {{coap-extensions-block}}).

* It updates the SCHC compression processing for the CoAP option OSCORE (see {{coap-extensions-oscore}}), in the light of recent developments related to the security protocol OSCORE as defined in {{I-D.ietf-core-oscore-key-update}} and {{I-D.ietf-core-oscore-groupcomm}}.

* It clarifies how the SCHC compression handles the CoAP payload marker (see {{payload-marker}}).

* It defines the SCHC compression of CoAP headers in the presence of CoAP proxies (see {{compression-with-proxies}}).

This document does not alter the core approach, design choices and features of the SCHC compression applied to CoAP headers.

## Terminology ## {#terminology}

The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT", "SHOULD", "SHOULD NOT", "RECOMMENDED", "NOT RECOMMENDED", "MAY", and "OPTIONAL" in this document are to be interpreted as described in BCP 14 {{RFC2119}} {{RFC8174}} when, and only when, they appear in all capitals, as shown here.

Readers are expected to be familiar with the terms and concepts related to the SCHC framework {{RFC8724}}, the web-transfer protocol CoAP {{RFC7252}}, the security protocol OSCORE {{RFC8613}} and the use of SCHC for CoAP {{RFC8824}}.

# CoAP Options ## {#coap-options}

This section updates and extends {{Section 5 of RFC8824}}, as to how SCHC compresses some specific CoAP options. In particular, {{coap-options-updated-set}} updates {{Section 5.4 of RFC8824}}.

## CoAP Option Size1, Size2, Proxy-URI, and Proxy-Scheme Fields ## {#coap-options-updated-set}

The SCHC Rule description MAY define sending some field values by describing an empty TV, with the MO set to "ignore" and the CDA set to "value-sent". A Rule MAY also use a "match-mapping" MO when there are different options for the same FID. Otherwise, the Rule sets the TV to the value, the MO to "equal", and the CDA to "not-sent".

## CoAP Option Hop-Limit Field ## {#coap-options-hop-limit}

The Hop-Limit field is an option defined in {{RFC8768}} that can be used to detect forwarding loops through a chain of CoAP proxies. The first proxy in the chain that understands the option includes it in a received request with a proper value set, before forwarding the request. Any following proxy that understands the option decrements the option value and forwards the request if the new value is different than zero, or returns a 5.08 (Hop Limit Reached) error response otherwise.

When a packet uses the Hop-Limit option, SCHC compression MUST send its content in the Compression Residue. The SCHC Rule describes an empty TV with the MO set to "ignore" and the CDA set to "value-sent".

## CoAP Option Echo Field ## {#coap-options-echo}

The Echo field is an option defined in {{RFC9175}} that a server can include in a response as a challenge to the client, and that the client echoes back to the server in one or more requests. This enables the server to verify the freshness of a request and to cryptographically verify the aliveness of the client. Also, it forces the client to demonstrate reachability at its claimed network address.

When a packet uses the Echo option, SCHC compression MUST send its content in the Compression Residue. The SCHC Rule describes an empty TV with the MO set to "ignore" and the CDA set to "value-sent".

## CoAP Option Request-Tag Field ## {#coap-options-request-tag}

The Request-Tag field is an option defined in {{RFC9175}} that the client can set in request messages of block-wise operations, with value an ephemeral short-lived identifier of the specific block-wise operation in question. This allows the server to match message fragments belonging to the same request operation and, if the server supports it, to reliably process simultaneous block-wise request operations on a single resource. If requests are integrity protected, this also protects against interchange of fragments between different block-wise request operations.

When a packet uses the Request-Tag option, SCHC compression MUST send its content in the Compression Residue. The SCHC Rule describes an empty TV with the MO set to "ignore" and the CDA set to "value-sent".

## CoAP Option EDHOC Field ## {#coap-options-edhoc}

The EDHOC field is an option defined in {{I-D.ietf-core-oscore-edhoc}} that a client can include in a request, in order to perform an optimized, shortened execution of the authenticated key establishment protocol EDHOC {{I-D.ietf-lake-edhoc}}. Such a request conveys both the final EDHOC message and actual application data, where the latter is protected with OSCORE {{RFC8613}} using a Security Context derived from the result of the current EDHOC execution.

The option occurs at most once and is always empty. The SCHC Rule MUST describe an empty TV, with the MO set to "equal" and the CDA set to "not-sent".

# SCHC Compression of CoAP Extensions {#coap-extensions}

This section updates and extends {{Section 6 of RFC8824}}, as to how SCHC compresses some specific CoAP options providing protocol extensions. In particular, {{coap-extensions-block}} updates {{Section 6.1 of RFC8824}}, while {{coap-extensions-oscore}} updates {{Section 6.4 of RFC8824}}.

## Block ## {#coap-extensions-block}

When a packet uses a Block1 or Block2 option {{RFC7959}} or a Q-Block1 or Q-Block2 option {{RFC9177}}, SCHC compression MUST send its content in the Compression Residue. The SCHC Rule describes an empty TV with the MO set to "ignore" and the CDA set to "value-sent". The Block1, Block2, Q-Block1 and Q-Block2 options allow fragmentation at the CoAP level that is compatible with SCHC fragmentation. Both fragmentation mechanisms are complementary, and the node may use them for the same packet as needed.

## OSCORE ## {#coap-extensions-oscore}

The security protocol OSCORE {{RFC8613}} provides end-to-end protection for CoAP messages. Group OSCORE {{I-D.ietf-core-oscore-groupcomm}} builds on OSCORE and defines end-to-end protection of CoAP messages in group communication {{I-D.ietf-core-groupcomm-bis}}. This section describes how SCHC Rules can be applied to compress messages protected with OSCORE or Group OSCORE.

{{fig-oscore-option}} shows the OSCORE option value encoding, which was originally defined in {{Section 6.1 of RFC8613}} and has been extended in {{I-D.ietf-core-oscore-key-update}}{{I-D.ietf-core-oscore-groupcomm}}. The first byte of the OSCORE option value specifies the content of the OSCORE option using flags, as follows.

* As defined in {{Section 4.1 of I-D.ietf-core-oscore-key-update}}, the eight least significant bit, when set, indicates that the OSCORE option includes a second byte of flags. The seventh least significant bit is currently unassigned.

* As defined in {{Section 5 of I-D.ietf-core-oscore-groupcomm}}, the sixth least significant bit, when set, indicates that the message including the OSCORE option is protected with the group mode of Group OSCORE (see {{Section 8 of I-D.ietf-core-oscore-groupcomm}}). When not set, the bit indicates that the message is protected either with OSCORE, or with the pairwise mode of Group OSCORE (see {{Section 9 of I-D.ietf-core-oscore-groupcomm}}), while the specific OSCORE Security Context used to protect the message determines which of the two cases applies.

* As defined in {{Section 6.1 of RFC8613}}, bit h, when set, indicates the presence of the kid context field in the option. Also, bit k, when set, indicates the presence of a kid field. Finally, the three least significant bits form the field n, which indicates the length of the piv (Partial Initialization Vector) field in bytes. When n = 0, no piv is present.

Assuming the presence of a single flag byte, this is followed by the piv field, the kid context field, and the kid field, in that order. Also, if present, the kid context field's length (in bytes) is encoded in the first byte, denoted by "s".

~~~~~~~~~~~
 0 1 2 3 4 5 6 7 <--------- n bytes ------------->
+-+-+-+-+-+-+-+-+---------------------------------+
|0 0 0|h|k|  n  |        Partial IV (if any)      |
+-+-+-+-+-+-+-+-+---------------------------------+
|               |                                 |
|<--   CoAP  -->|<------- CoAP OSCORE_piv ------> |
   OSCORE_flags

 <-- 1 byte --> <------ s bytes ----->
+--------------+----------------------+-----------------------+
|  s (if any)  | kid context (if any) | kid (if any)      ... |
+--------------+----------------------+-----------------------+
|                                     |                       |
|<-------- CoAP OSCORE_kidctx ------->|<-- CoAP OSCORE_kid -->|
~~~~~~~~~~~
{: #fig-oscore-option title="OSCORE Option" artwork-align="center"}

{{fig-oscore-option-kudos}} shows the OSCORE option value encoding, with the second byte of flags also present. As defined in {{Section 4.1 of I-D.ietf-core-oscore-key-update}}, the least significant bit d of this byte, when set, indicates that two additional fields are included in the option, following the kid context field (if any).

These two fields, namely x and nonce, are used when running the key update protocol KUDOS defined in {{I-D.ietf-core-oscore-key-update}}, with x specifying the length of the nonce field in bytes as well as the specific behavior to adopt during the KUDOS execution. In particular, the figure provides the breakdown of the x field, where its three least significant bits form the sub-field m, which specifies the size of nonce in bytes, minus 1.

~~~~~~~~~~~
 0 1 2 3 4 5 6 7  8   9   10  11  12  13  14  15 <----- n bytes ----->
+-+-+-+-+-+-+-+-+---+---+---+---+---+---+---+---+---------------------+
|1|0|0|h|k|  n  | 0 | 0 | 0 | 0 | 0 | 0 | 0 | d | Partial IV (if any) |
+-+-+-+-+-+-+-+-+---+---+---+---+---+---+---+---+---------------------+
|                                               |                     |
|<------------------- CoAP -------------------->|<- CoAP OSCORE_piv ->|
                   OSCORE_flags

 <- 1 byte -> <----------- s bytes ------------> <------ 1 byte ----->
+------------+----------------------------------+---------------------+
| s (if any) |       kid context (if any)       |     x (if any)      |
+------------+----------------------------------+---------------------+
|                                               |                     |
|<------------- CoAP OSCORE_kidctx ------------>|<-- CoAP OSCORE_x -->|
                                                |                     |
                                                |   0 1 2 3 4 5 6 7   |
                                                |  +-+-+-+-+-+-+-+-+  |
                                                |  |0|0|b|p|   m   |  |
                                                |  +-+-+-+-+-+-+-+-+  |

 <----- m + 1 bytes ----->
+-------------------------+-----------------------+
|      nonce (if any)     |    kid (if any) ...   |
+-------------------------+-----------------------+
|                         |                       |
|<-- CoAP OSCORE_nonce -->|<-- CoAP OSCORE_kid -->|
~~~~~~~~~~~
{: #fig-oscore-option-kudos title="OSCORE Option during a KUDOS execution" artwork-align="center"}

To better perform OSCORE SCHC compression, the Rule description needs to identify the OSCORE option and the fields it contains. Conceptually, it discerns up to six distinct pieces of information within the OSCORE option: the flag bits, the piv, the kid context, the x byte, the nonce, and the kid. The SCHC Rule splits the OSCORE option into six Field Descriptors in order to compress them:

* CoAP OSCORE_flags
* CoAP OSCORE_piv
* CoAP OSCORE_kidctx
* CoAP OSCORE_x
* CoAP OSCORE_nonce
* CoAP OSCORE_kid

{{fig-oscore-option}} shows the OSCORE option format with the four fields OSCORE_flags, OSCORE_piv, OSCORE_kidctx and OSCORE_kid superimposed on it. Also, {{fig-oscore-option-kudos}} shows the OSCORE option format with all the six fields superimposed on it, with reference to a message exchanged during an execution of the KUDOS key update protocol.

In both cases, the CoAP OSCORE_kidctx field directly includes the size octet, s. In the latter case, the following applies.

* For the x field, if both endpoints know the value, then the SCHC Rule will describe a TV to this value, with the MO set to "equal" and the CDA set to "not-sent". This models the case where the two endpoints run KUDOS with a pre-agreed size of the nonce field, as well as with a pre-agreed combination of its modes of operations, as per the bits b and p of the m sub-field.

   Otherwise, if the value is changing over time, the SCHC Rule will set the MO to "ignore" and the CDA to "value-sent". The Rule may also use a "match-mapping" MO to compress this field, in case the two endpoints pre-agree on a set of alternative ways to run KUDOS, with respect to the size of the nonce field and the combination of the KUDOS modes of operation to use.

* For the nonce field, the SCHC Rule describes an empty TV with the MO set to "ignore" and the CDA set to "value-sent".

   In addition, for the value of the nonce field, SCHC MUST NOT send it as variable-length data in the Compression Residue, to avoid ambiguity with the length of the nonce field encoded in the x field. Therefore, SCHC MUST use the m sub-field of the x field to define the size of the Compression Residue. SCHC designates a specific function, "osc.x.m", that the Rule MUST use to complete the Field Descriptor. During the decompression, this function returns the length of the nonce field in bytes, as the value of the three least significant bits of the m sub-field of the x field, plus 1.

# Compression of the CoAP Payload Marker {#payload-marker}

As originally intended in {{RFC8824}}, the following applies with respect to the 0xFF payload marker. A SCHC compression rule for CoAP includes all the expected CoAP options, therefore the payload marker does not have to be specified.

## Without End-to-End Security ## {#payload-marker-without-e2e}

If the CoAP message to compress with SCHC is not going to be protected with OSCORE and includes a payload, then the 0xFF payload marker MUST NOT be included in the compressed message, which is composed of the Compression RuleID, the Compression Residue (if any), and the CoAP payload.

After having decompressed an incoming message, the recipient endpoint MUST prepend the 0xFF payload marker to the CoAP payload, if any was present after the consumed Compression Residue.

## With End-to-End Security

If the CoAP message has to be protected with OSCORE, the same rationale described in {{payload-marker-without-e2e}} applies to both the Inner SCHC Compression and the Outer SCHC Compression defined in {{Section 7.2 of RFC8824}}. That is:

* After the Inner SCHC Compression of a CoAP message including a payload, the payload marker MUST NOT be included in the input to the AEAD Encryption, which is composed of the Inner Compression RuleID, the Inner Compression Residue (if any), and the CoAP payload.

* The Outer SCHC Compression takes as input the OSCORE-protected message, which always includes a payload (i.e., the OSCORE Ciphertext) preceded by the payload marker.

* After the Outer SCHC Compression, the payload marker MUST NOT be included in the final compressed message, which is composed of the Outer Compression RuleID, the Outer Compression Residue (if any), and the OSCORE Ciphertext.

After having completed the Outer SCHC Decompression of an incoming message, the recipient endpoint MUST prepend the 0xFF payload marker to the OSCORE Ciphertext.

After having completed the Inner SCHC Decompression of an incoming message, the recipient endpoint MUST prepend the 0xFF payload marker to the CoAP payload, if any was present after the consumed Compression Residue.

# CoAP Header Compression with Proxies ## {#compression-with-proxies}

Building on {{RFC8824}}, this section clarifies how SCHC Compression/Decompression is performed when CoAP proxies are deployed. The following refers to the origin client and origin server as application endpoints.

## Without End-to-End Security

In case OSCORE is not used end-to-end between client and server, the SCHC processing occurs hop-by-hop, by relying on SCHC Rules that are consistently shared between two adjacent hops.

In particular, SCHC is used as defined below.

* The sender application endpoint compresses the CoAP message, by using the SCHC Rules that it shares with the next hop towards the recipient application endpoint. The resulting, compressed message is sent to the next hop towards the recipient application endpoint.

* Each proxy decompresses the incoming compressed message, by using the SCHC Rules that it shares with the (previous hop towards the) sender application endpoint.

   Then, the proxy compresses the CoAP message to be forwarded, by using the SCHC Rules that it shares with the (next hop towards the) recipient application endpoint.

   The resulting, compressed message is sent to the (next hop towards the) recipient application endpoint.

* The recipient application endpoint decompresses the incoming compressed message, by using the SCHC Rules that it shares with the previous hop towards the sender application endpoint.

## With End-to-End Security

In case OSCORE is used end-to-end between client and server (see {{Section 7.2 of RFC8824}}), the following applies.

The SCHC processing occurs end-to-end as to the Inner SCHC Compression/Decompression, by relying on Inner SCHC Rules that are consistently shared between the two application endpoints acting as OSCORE endpoints and sharing the used OSCORE Security Context.

Instead, the SCHC processing occurs hop-by-hop as to the Outer SCHC Compression/Decompression, by relying on Outer SCHC Rules that are consistently shared between two adjacent hops.

In particular, SCHC is used as defined below.

* The sender application endpoint performs the Inner SCHC Compression on the original CoAP message, by using the Inner SCHC Rules that it shares with the recipient application endpoint.

   Following the AEAD Encryption of the compressed input obtained from the previous step, the sender application endpoint performs the Outer SCHC Compression on the resulting OSCORE-protected message, by using the Outer SCHC Rules that it shares with the next hop towards the recipient application endpoint.

   The resulting, compressed message is sent to the next hop towards the recipient application endpoint.

* Each proxy performs the Outer SCHC Decompression on the incoming compressed message, by using the SCHC Rules that it shares with the (previous hop towards the) sender application endpoint.

   Then, the proxy performs the Outer SCHC Compression of the OSCORE-protected message to be forwarded, by using the SCHC Rules that it shares with the (next hop towards the) recipient application endpoint.

   The resulting, compressed message is sent to the (next hop towards the) recipient application endpoint.

* The recipient application endpoint performs the Outer SCHC Decompression on the incoming compressed message, by using the Outer SCHC Rules that it shares with the previous hop towards the sender application endpoint.

   Then, the recipient application endpoint performs the AEAD Decryption of the OSCORE-protected message obtained from the previous step.

   Finally, the recipient application endpoint performs the Inner SCHC Decompression on the compressed input obtained from the previous step, by using the Inner SCHC rules that it shares with the sender application endpoint. The result is the original CoAP message produced by the sender application endpoint.

# Examples of CoAP Header Compression with Proxies ## {#examples}

TBD

# Security Considerations

The security considerations discussed in {{RFC8724}} and {{RFC8824}} continue to apply.  When SCHC is used in the presence of CoAP proxies, the security considerations discussed in {{Section 11.2 of RFC7252}} continue to apply. When SCHC is used with OSCORE, the security considerations discussed in {{RFC8613}} continue to apply.

The security considerations in {{RFC8824}} specifically discuss how the use of SCHC for CoAP when OSCORE is also used may result in (more frequently) triggering key-renewal operations for the two endpoints. This can be due to an earlier exhaustion of the OSCORE Sender Sequence Number space, or to the installation of new compression Rules on one of the endpoints.

In either case, the two endpoints can run the key update protocol KUDOS defined in {{I-D.ietf-core-oscore-key-update}}, as the recommended method to update their shared OSCORE Security Context.

# IANA Considerations

This document has no actions for IANA.

--- back

# YANG data model

TBD

# Acknowledgments # {#acknowledgments}
{: numbered="no"}

The authors sincerely thank {{{Göran Selander}}} for his comments and feedback.

The work on this document has been partly supported by the H2020 projects SIFIS-Home (Grant agreement 952652) and ARCADIAN-IoT (Grant agreement 101020259).
